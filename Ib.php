<?
/*
 * Класс-обертка get/set-функций Битрикс,
 * для упрощения синтаксиса фреймворка
 */

abstract class Ib
{
    protected $iIBlockId = null;

    public function __construct()
    {
        if (!$this->iIBlockId)
            throw new Exception( 'Не указан IBLOCK_ID');
    }

    /*
     * GetList обертка
     */
    public function Get($aFilter = [], $iLimit = 1, $aSelect = [], $aSort = [], $sKey = null)
    {
        $arr = [];

        if (!is_array($aFilter))
            if ((int)$aFilter > 0)
                $aFilter = array('ID' => (int)$aFilter);
            else
                return false; // wrong id

        $aLimit = ( (int)$iLimit > 0 ) 
            ? array('nTopCount'=>$iLimit) 
            : false;


        $res = CIBlockElement::GetList(
            $aSort,
            array_merge(['IBLOCK_ID' => $this->iIBlockId, 'ACTIVE' => 'Y'], $aFilter),
            false,
            $aLimit,
            $aSelect
        );
        while ($item = $res->GetNextElement()) {
            $fields = $item->GetFields();
            $props = $item->GetProperties();
            foreach ($props as $k => $v) {
                $fields[$k] = $v['VALUE'];
                if ($v['DESCRIPTION'])
                    $fields[$k . '_description'] = $v['DESCRIPTION'];
            }
            if ($sKey)
                $arr[$fields[$sKey]] = $fields;
            else
                $arr[] = $fields;
        }

        return ($iLimit == 1) 
            ? array_shift($arr) 
            : $arr;
    }

    public function Update($element_id, $aKeyValue = [])
    {
        if ($element_id AND $aKeyValue)
        {
            $el = new CIBlockElement;
            $res = $el->Update($id, $aKeyValue);
            return $res;
        }
        else
            return false;
    }

    public function UpdateProp($prop_id, $aKeyValue = [])
    {
        if ($prop_id AND $aKeyValue)
            foreach ($aKeyValue as $k => $v)
                CIBlockElement::SetPropertyValuesEx($id, false, array($k => $v));
    }

    public function UpdatePropHtml($prop_id, $aKeyValue = [])
    {
        if ($prop_id AND $aKeyValue)
            foreach ($aKeyValue as $k => $v)
                CIBlockElement::SetPropertyValuesEx($prop_id, false, array($k => array('VALUE' => array('TYPE' => 'HTML', 'TEXT' => $v))));
    }

}