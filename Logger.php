<?php
/*
 * Logger - write strings to file, for debug
 * $log = new Logger(); 
 * $log->info('Debug info');
 */

class Logger
{
    private $filename;
    private $delim;
    private $headers;
    private $console_output = false;


    // CONSTRUCTOR
    function __construct($filename = null, $delim = ';', $console_need = false)
    {
        $this->filename = $filename ?: debug_backtrace()[0]['file'] . '.log';
        $this->delim = $delim;
        $this->console_output = $console_need;
        $this->headers =
            'DATETIME' . $this->delim .
            'ERRORLEVEL' . $this->delim .
            'VALUE' . $this->delim .
            'LINE' . $this->delim .
            'FILE';
    }


    // write str to $filename
    private function log($errorlevel = 'INFO', $value = '')
    {
        $path = pathinfo($this->filename, PATHINFO_DIRNAME);
        if (!is_dir($path))
            mkdir($path, 0777, true);

        $datetime = date("Y-m-d H:i:s");
        $headers = (!file_exists($this->filename)) 
            ? $this->headers . "\n" 
            : null;

        $fd = fopen($this->filename, "a");

        if ($headers) {
            fwrite($fd, $headers);
        }

        $debugBacktrace = debug_backtrace();
        $line = $debugBacktrace[1]['line'];
        $file = basename($debugBacktrace[1]['file']);
        $file_line = $file . ':' . $line;

        $entry = array($datetime, $errorlevel, $value, $file_line);

        fputcsv($fd, $entry, $this->delim);

        fclose($fd);

        if ($this->console_output)
            echo implode($this->delim, $entry) . "\n";
    }


    function info($value = '')
    {
        self::log('INFO', $value);
    }


    function warning($value = '', $tag = self::DEFAULT_TAG)
    {
        self::log('WARNING', $value, $tag);
    }


    function error($value = '', $tag = self::DEFAULT_TAG)
    {
        self::log('ERROR', $value, $tag);
    }

    function debug($value = '', $tag = self::DEFAULT_TAG)
    {
        self::log('DEBUG', $value, $tag);
    }

}
