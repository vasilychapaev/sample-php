CREATE TABLE clients (
  id int(11) NOT NULL AUTO_INCREMENT,
  last_name varchar(100) NOT NULL,
  first_name varchar(100) NOT NULL,
  phone varchar(20) NOT NULL,
  email varchar(100) NOT NULL,
  PRIMARY KEY (id),
  KEY last_name (last_name),
  KEY phone (phone),
  KEY email (email)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO clients (id, last_name, first_name, phone, email) VALUES
(1,	'Васильев',	'Вася',	'80291234455',	'vasya@tut.by'),
(2,	'Петров',	'Петя',	'+375296665544',	'petro@mail.ru');

CREATE TABLE orders (
  id int(11) NOT NULL AUTO_INCREMENT,
  client_id int(11) NOT NULL,
  status_id tinyint(4) NOT NULL,
  comment text NOT NULL,
  ordered_at datetime NOT NULL,
  PRIMARY KEY (id),
  KEY client_id (client_id),
  KEY status_id (status_id),
  KEY client_id_status_id (client_id,status_id),
  KEY ordered_at (ordered_at)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO orders (id, client_id, status_id, comment, ordered_at) VALUES
(1,	2,	1,	'кубик-рубик нужен одноцветный. говорят, такой проще собирать',	'2018-04-14 18:04:12');

CREATE TABLE order_items (
  id int(11) NOT NULL AUTO_INCREMENT,
  order_id int(11) NOT NULL,
  product_id int(11) NOT NULL,
  cnt int(11) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO order_items (id, order_id, product_id, cnt) VALUES
(1,	1,	1,	3),
(2,	1,	2,	5);

CREATE TABLE order_statuses (
  id int(11) NOT NULL AUTO_INCREMENT,
  title varchar(20) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO order_statuses (id, title) VALUES
(1,	'новый'),
(2,	'оплачен'),
(3,	'доставлен');

CREATE TABLE products (
  id int(11) NOT NULL AUTO_INCREMENT,
  title varchar(100) NOT NULL,
  price float NOT NULL,
  currency_id int(11) NOT NULL,
  cnt int(11) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO products (id, title, price, currency_id, cnt) VALUES
(1,	'Кубик',	10,	1,	5),
(2,	'Рубик',	28,	2,	13);

